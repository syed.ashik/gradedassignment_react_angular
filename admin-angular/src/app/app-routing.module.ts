import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { CrudAddComponent } from './components/crud-add/crud-add.component';
import { CrudUpdateComponent } from './components/crud-update/crud-update.component';
import { CrudDeleteComponent } from './components/crud-delete/crud-delete.component';
import { CrudViewAllUsersComponent } from './components/crud-view-all-users/crud-view-all-users.component';
import { ViewOrdersComponent } from './components/view-orders/view-orders.component';


const routes: Routes = [
  {path:'', component: LoginComponent},
  {path:'adminhome', component: HomeComponent},
  {path:'crudAdd', component: CrudAddComponent},
  {path:'crudUpdate', component: CrudUpdateComponent},
  {path:'crudDelete', component: CrudDeleteComponent},
  {path:'crudViewUsers', component: CrudViewAllUsersComponent},
  {path:'orders', component: ViewOrdersComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
