import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { CrudAddComponent } from './components/crud-add/crud-add.component';
import { CrudUpdateComponent } from './components/crud-update/crud-update.component';
import { CrudDeleteComponent } from './components/crud-delete/crud-delete.component';
import { CrudViewAllUsersComponent } from './components/crud-view-all-users/crud-view-all-users.component';
import { ViewOrdersComponent } from './components/view-orders/view-orders.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    CrudAddComponent,
    CrudUpdateComponent,
    CrudDeleteComponent,
    CrudViewAllUsersComponent,
    ViewOrdersComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
