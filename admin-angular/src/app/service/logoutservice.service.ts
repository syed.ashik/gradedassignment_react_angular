import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Admin } from '../model/Admin';

@Injectable({
  providedIn: 'root'
})
export class LogoutserviceService {

  constructor(private _http:HttpClient) { }
  
  logoutAdmin(admin:Admin):Observable<any>{
    admin.adminId = Number(localStorage.getItem('adminId'));
    return this._http.get(`http://localhost:8080/surabi/admin/${admin.adminId}/logout`, {responseType: 'text'});
  }

}