import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Admin } from '../model/Admin';

@Injectable({
  providedIn: 'root'
})

export class LoginserviceService {

  constructor(private _http:HttpClient) { }
  
  loginAdmin(admin:Admin):Observable<any>{
    return this._http.post("http://localhost:8080/surabi/admin/login", admin);
  }

}
