import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Admin } from '../model/Admin';


@Injectable({
  providedIn: 'root'
})

export class BillserviceService {

  constructor(private _http:HttpClient) { }

  getTodaySales(admin:Admin):Observable<any>{
    admin.adminId = Number(localStorage.getItem('adminId'));
    return this._http.get(`http://localhost:8080/surabi/admin/${admin.adminId}/today-sales`);
  }

  getMonthSales(admin:Admin):Observable<any>{
    admin.adminId = Number(localStorage.getItem('adminId'));
    return this._http.get(`http://localhost:8080/surabi/admin/${admin.adminId}/month-sales`);
  }

}