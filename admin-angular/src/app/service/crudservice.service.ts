import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../model/User';

@Injectable({
  providedIn: 'root'
})
export class CrudserviceService {

  constructor(private _http:HttpClient) { }
  
  getAllUsers():Observable<any>{
    return this._http.get("http://localhost:8080/surabi/admin/users");
  }

  addUser(user:User):Observable<any>{
    return this._http.post("http://localhost:8080/surabi/admin/add-user", user);
  }

  updateUser(user:User):Observable<any>{
    return this._http.put("http://localhost:8080/surabi/admin/update-user", user);
  }

  deleteUser(user:User):Observable<any>{
    return this._http.delete(`http://localhost:8080/surabi/admin/users/${user.userId}`);
  }
}
