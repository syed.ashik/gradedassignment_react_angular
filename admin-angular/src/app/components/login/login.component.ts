import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Admin } from 'src/app/model/Admin';
import { NgForm } from '@angular/forms';
import { LoginserviceService } from 'src/app/service/loginservice.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  
  errorLogin: boolean = false;
  admin = new Admin();
  constructor(private _service:LoginserviceService, private _route:Router) { }

  ngOnInit(){
  }

  onLogin(){
    this._service.loginAdmin(this.admin).subscribe(
      
      data => {
        this.admin.adminId = data.adminId;
        this.admin.adminLogIn = data.adminLogIn;
        
        localStorage.setItem('adminId',this.admin.adminId.toString());
        localStorage.setItem('adminLogIn',this.admin.adminLogIn.toString());
        //console.log(this.admin);
        this.errorLogin = false;
        this._route.navigate(['/adminhome']);
      },

      error => {
        this.errorLogin = true;
        console.log(error.error.text);
      }
    )
  }

}
