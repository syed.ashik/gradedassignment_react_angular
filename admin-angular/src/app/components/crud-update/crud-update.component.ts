import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/model/User';
import { CrudserviceService } from 'src/app/service/crudservice.service';


@Component({
  selector: 'app-crud-update',
  templateUrl: './crud-update.component.html',
  styleUrls: ['./crud-update.component.css']
})
export class CrudUpdateComponent implements OnInit {
  
  updateMsg : boolean = false;
  errorUpdate : boolean = false;
  user = new User();
  
  constructor(private _service:CrudserviceService, private _route:Router) { }

  ngOnInit(): void {
  }

  updateUser(){
    this._service.updateUser(this.user).subscribe(
      data => {
        this.errorUpdate = false;
        this.updateMsg = true;
        console.log(data);
      },
      error => {
        this.updateMsg = false;
        this.errorUpdate = true;
        console.log(error.error.text);
      }
    )
  }

  gotoHome(){
    this._route.navigate(['/adminhome']);
  }

}
