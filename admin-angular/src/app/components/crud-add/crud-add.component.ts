import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/model/User';
import { CrudserviceService } from 'src/app/service/crudservice.service';


@Component({
  selector: 'app-crud-add',
  templateUrl: './crud-add.component.html',
  styleUrls: ['./crud-add.component.css']
})
export class CrudAddComponent implements OnInit {
  
  addMsg : boolean = false;
  errorAdd : boolean = false;
  user = new User();
  
  constructor(private _service:CrudserviceService, private _route:Router) { }

  ngOnInit(): void {
  }

  addUser(){
    this._service.addUser(this.user).subscribe(
      data => {
        this.errorAdd = false;
        this.addMsg = true;
        console.log(data);
      },
      error => {
        this.addMsg = false;
        this.errorAdd = true;
        console.log(error.error.text);
      }
    ) 
  }

  gotoHome(){
    this._route.navigate(['/adminhome']);
  }

}
