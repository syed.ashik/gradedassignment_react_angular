import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/model/User';
import { CrudserviceService } from 'src/app/service/crudservice.service';


@Component({
  selector: 'app-crud-delete',
  templateUrl: './crud-delete.component.html',
  styleUrls: ['./crud-delete.component.css']
})
export class CrudDeleteComponent implements OnInit {
  
  deleteMsg : boolean = false;
  errorDelete : boolean = false;
  error2 : boolean = false;

  user = new User();
  
  constructor(private _service:CrudserviceService, private _route:Router) { }


  ngOnInit(): void {
  }

  deleteUser(){
    this._service.deleteUser(this.user).subscribe(
      data => {
        console.log(data);
        this.errorDelete = false;
        this.error2 = false;
        this.deleteMsg = true;
      },
      error => { 
        console.log("err-"+error.error.text);
        this.deleteMsg = false;
        
        if(error.error.text === undefined){
          this.errorDelete = false;
           this.error2 = true;
        }
        else{
          this.error2 = false;
          this.errorDelete = true; 
        }     
      }
    )
  }

  gotoHome(){
    this._route.navigate(['/adminhome']);
  }

}
