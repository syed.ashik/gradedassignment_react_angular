import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Admin } from 'src/app/model/Admin';
import { Order } from 'src/app/model/Order';
import { BillserviceService } from 'src/app/service/billservice.service';


@Component({
  selector: 'app-view-orders',
  templateUrl: './view-orders.component.html',
  styleUrls: ['./view-orders.component.css']
})

export class ViewOrdersComponent implements OnInit {

  constructor(private _service:BillserviceService, private _route:Router) { }
  
  admin = new Admin();
  order = new Order();
  orders:Order [] = new Array<Order>();
  showTodayOrders:boolean = false;
  showMonthOrders:boolean = false;
  noSales:boolean = false;

  ngOnInit(): void {
  }

  showTodaySales(){
    this.order.todayIncome = 0;
    this.showMonthOrders = false;
    this._service.getTodaySales(this.admin).subscribe(
        
      data => {

          if(data !== null){
            this.noSales = false;
            this.orders = data;
            this.orders.forEach(item => {
              this.order.todayIncome += item.billAmount;
            });
            this.showTodayOrders = true;
          }
          else{
            this.noSales = true;
            this.showTodayOrders = false;
          }

        },

      error => console.log(error.error.text)
    )    
  }

  showMonthSales(){
    this.order.monthIncome = 0;
    this.showTodayOrders = false;
    this._service.getMonthSales(this.admin).subscribe(

      data => {
        
          if(data !== null){
            this.noSales = false;
            this.orders = data;
            this.orders.forEach(item => {
              this.order.monthIncome += item.billAmount;
            });
            this.showMonthOrders = true;
          }
          else{
            this.noSales = true;
            this.showMonthOrders = false;
          }

      },

      error => console.log(error.error.text)
    ) 
  }

  gotoHome(){
    this._route.navigate(['/adminhome']);
  }

}
