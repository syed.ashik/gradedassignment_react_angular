import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CrudViewAllUsersComponent } from './crud-view-all-users.component';

describe('CrudViewAllUsersComponent', () => {
  let component: CrudViewAllUsersComponent;
  let fixture: ComponentFixture<CrudViewAllUsersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CrudViewAllUsersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CrudViewAllUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
