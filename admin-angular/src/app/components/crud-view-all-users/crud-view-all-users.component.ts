import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/model/User';
import { CrudserviceService } from 'src/app/service/crudservice.service';


@Component({
  selector: 'app-crud-view-all-users',
  templateUrl: './crud-view-all-users.component.html',
  styleUrls: ['./crud-view-all-users.component.css']
})
export class CrudViewAllUsersComponent implements OnInit {

  user = new User();
  users:User [] = new Array<User>();
  
  constructor(private _service:CrudserviceService, private _route:Router) { }

  ngOnInit(): void {
    this.viewAllUsers();
  }

  viewAllUsers(){
    this._service.getAllUsers().subscribe(
      data => {
        console.log(data);
        this.users = data;
      },
      error => console.log(error.error.text)
    )
  }

  gotoHome(){
    this._route.navigate(['/adminhome']);
  }

}
