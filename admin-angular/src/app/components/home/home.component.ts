import { Component, OnInit, Input, ElementRef} from '@angular/core';
import { Router } from '@angular/router';
import { LogoutserviceService } from 'src/app/service/logoutservice.service';
import { Admin } from 'src/app/model/Admin';
import { User } from 'src/app/model/User';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {
  
  admin = new Admin();
  user = new User();
  
  constructor(private _logoutservice:LogoutserviceService, private _route:Router) { }

  ngOnInit(): void {
  }

  gotoCrudViewAll(){
    this._route.navigate(['/crudViewUsers']);
  }
  
  gotoCrudAdd(){
    this._route.navigate(['/crudAdd']);
  }

  gotoCrudUpdate(){
    this._route.navigate(['/crudUpdate']);
  }

  gotoCrudDelete(){
    this._route.navigate(['/crudDelete']);
  }

  gotoOrders(){
    this._route.navigate(['/orders']);
  }
 

  onLogout(){
    this._logoutservice.logoutAdmin(this.admin).subscribe(
      data => {
        console.log(data);
        this._route.navigate(['/']);
      },
      error => console.log("err-"+error.error.text)
    )
    
  }

}
