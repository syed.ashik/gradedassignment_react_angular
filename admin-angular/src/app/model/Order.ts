export class Order{

    orderId:number = 0;
    userId:number = 0;
    orderDate:string = "";
    orderTime:string = "";
    billAmount:number = 0;
    todayIncome:number = 0;
    monthIncome:number = 0;

}