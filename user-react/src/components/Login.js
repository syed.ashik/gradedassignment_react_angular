import React, { useState } from 'react';
import {Row, Col, Container} from 'react-bootstrap';

const Login = (props) => {

    const [userName, setName] = useState('');
    const [userPassword, setPassword] = useState('');
    const {loadUser, onRouteChange} = props;


//getting username as input
  function onNameChange(event){
    setName(event.target.value);
  }

//getting password as input
  const onPasswordChange = (event) => {
    setPassword(event.target.value);
  }


//user clicking on Login button - backend connection
  const onSubmitLogIn = async() => {
    let userData = {
      userName: userName,
      userPassword: userPassword 
    }

    await fetch('http://localhost:8080/surabi/users/login', {
      method: 'POST',
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify(
        userData
      )
    })

    .then(
      response => response.json()
    )
    .then(
      data => { document.getElementById('loginerr').style.display='none';
                console.log(data);
                loadUser(data.userId);
                onRouteChange('menu');
              }
    )

    .catch(err => {
      document.getElementById('loginerr').style.display='block';
    })
  }



  return(
    <Container >
      <Row className='justify-content-md-center'>
        <Col md={4} style={{marginTop:'50px', border: '3px solid darkgray', borderRadius:'10px', padding:'40px'}}>
            <legend style={{textAlign:'center', fontSize:'40px', padding:'3px', fontWeight:'bold'}}>LOGIN</legend>

            <Row className='justify-content-md-center' 
                 style={{padding:'5px', margin:'10px'}}>
              <label style={{fontSize:'20px'}}>Username</label>
              <input
                type="name"
                name="username"
                id="username"
                onChange={onNameChange}
                style={{fontSize:'15px'}}
              />
            </Row>
            <Row className='justify-content-md-center' 
                 style={{padding:'5px', margin:'10px'}}>
              <label style={{fontSize:'20px'}}>Password</label>
              <input
                type="password"
                name="password"
                id="password"
                onChange={onPasswordChange}
                style={{fontSize:'15px'}}
              />
            </Row>

            <Row className='justify-content-md-center'>
            <input
              onClick={onSubmitLogIn}
              type="submit"
              value="Log In"
              style={{color:'whitesmoke', fontSize:'20px', backgroundColor:'indigo', borderRadius:'5px', marginTop:'20px', height:'45px', width:'110px'}}
            />
            </Row>

{/*go to register page if new user*/}
            <Row>
               <p onClick={() => onRouteChange('register')}  style={{textAlign:'center', color:'gray', fontSize:'15px', marginTop:'10px', cursor:'pointer'}}
                >New user? Register</p>
            </Row>

{/*display error for invalid credentials*/}
            <Row className='justify-content-md-center'>
                <p id='loginerr' style={{display:'none', fontSize:'20px', marginTop:'10px', color:'red', textAlign:'center'}}>Invalid credentials!</p>
            </Row>
    </Col>
    </Row>
    </Container>
  );
}
export default Login;
