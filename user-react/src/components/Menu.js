import React, { useState, useEffect } from 'react';
import {Row, Col, Container, Button, Accordion, Alert, Table} from 'react-bootstrap';

const Menu = (props) => {
    useEffect( ()=>{
       getData();
    },[]);

  const [items, setItems] = useState([]);
  const [order, setOrder] = useState({});
  const [billAmount, setBillAmount] = useState(0);
  const [result, setAlert] = useState(false);
  const {userId, onRouteChange} = props;
  
  const showResult = () => {
		setAlert(true);
	};

  const hideResult = () => {
		setAlert(false);
	};

//display bill
  const viewBill = () => {
    document.getElementById('bill').style.display = 'inline-block';
  }

//get menu items from backend
  const getData = () => {
    fetch('http://localhost:8080/surabi/users/menu')
      .then(response => response.json())
      .then(data => {
         setItems(data);
      });
  }


//on clicking place order button
  async function onPlaceOrder(){
    
    let orderData = {};
    let amt = 0;
    for(var i=1; i<=items.length; i++){

      var itemQuantity = document.getElementById(i);

        if(itemQuantity.value === '')
          orderData[i]=0;
        else{
          orderData[i]=parseInt(itemQuantity.value);
          amt += items[i-1].price * orderData[i];
          
        }
    }
    
    setOrder(orderData);
    setBillAmount(amt);

//if order amount>0 then place order
    if(amt > 0){
      await fetch('http://localhost:8080/surabi/users/'+ userId +'/order', {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(
          orderData
        )
      })
    }
  showResult();
    //console.log(order);
  }

  
  //user clicking on Logout button - backend connection
  const onLogout = async() => {
    await fetch('http://localhost:8080/surabi/users/'+ userId +'/logout')
      .then(data => {
          onRouteChange('login');
      })
      .catch(err => {
        console.log(err);
    })
  }

     return (
      <Container>
              <Row className='justify-content-md-end'>
                <Col sm={2}>
                  <p onClick={onLogout}
                     style={{fontSize:'30px', color:'indigo', marginTop:'8px', textDecoration:'underline', cursor: 'pointer'}}
                  >Log Out
                  </p>
                </Col>
              </Row>
              <Row className='justify-content-md-center'>
                <Col md={2}>
                <legend style={{textAlign:'center', fontSize:'40px', padding:'3px', fontWeight:'bold'}}>MENU</legend>
                </Col>
              </Row>
              
              {items.map((item,index)=>{
                return(
                  <Row className='justify-content-md-center'>
                    <Col md={6}>
{/*display menu items*/}
               <Accordion>
                  <Accordion.Item eventKey={index}>
              
                    <Accordion.Header>{item.itemName}
                    </Accordion.Header>

                    <Accordion.Body>
                      <b>Price: </b>{item.price}
                    </Accordion.Body>
                  </Accordion.Item>
                </Accordion>
                </Col>
                <Col md={2}>
                <input id={index+1} type="text" placeholder="Enter quantity..." style={{marginTop:'10px', width:'140px'}}/>
                </Col>
                </Row>)
              })
              }
              
              <Row className='justify-content-md-center'>
              <Col md={8}>
                {
                  (billAmount>0)
                  ?(<Alert show={result} variant='success' style={{marginTop:'20px', textAlign:'center'}}>
                      <h5>Your order placed successfully!</h5>
                      <Button onClick={() => viewBill()} variant='dark' style={{fontSize:'16px', height:'35px', width:'120px'}}>View Bill
                      </Button>
                    </Alert>)
                  :(<Alert show={result} variant='secondary' style={{marginTop:'20px', textAlign:'center'}}>
                      <h5>No items added to cart</h5>
                      <Button onClick={() => hideResult()} variant='danger' style={{fontSize:'16px', height:'35px', width:'100px'}}>Close
                      </Button>
                    </Alert>)
                }
              </Col>
              </Row>
              
{/*Table to display bill*/}
              <Row className='justify-content-md-center'>
                <Col md={4}>
                  <Table striped bordered hover id='bill' style={{display:'none', textAlign:'center'}}>
                      <thead>
                        <tr>
                          <th>Seq.No.</th>
                          <th>Item Name</th>
                          <th>Quantity</th>
                          <th>Price</th>
                        </tr>
                      </thead>
                      <tbody>
                      {Object.keys(order).map((item)=>{
                        if(order[item] !== 0)
                            return(
                            <tr>
                              <td>{item}</td>
                              <td>{items[item-1].itemName}</td>
                              <td>{order[item]}</td>
                              <td>{items[item-1].price * order[item]}</td>
                            </tr>)
                        })
                      }
                      <tr>
                        <td></td>
                        <td></td>
                        <th>Total Amount</th>
                        <th>Rs.{billAmount}</th>
                      </tr>
                      </tbody>
                  </Table>
                </Col>
              </Row>

              <Row className='justify-content-md-center'>
                <Col sm={2}>
              <input id='btnOrder'
                onClick={onPlaceOrder}
                type="submit"
                value="Place Order"
                style={{color:'whitesmoke', backgroundColor:'indigo', borderRadius:'3px', marginTop:'10px', marginBottom:'5px', fontSize:'20px', height:'50px', width:'160px'}}
              />
              </Col>
              </Row>
      </Container>
    );
}
export default Menu;
