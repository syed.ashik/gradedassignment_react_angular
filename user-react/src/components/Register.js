import React, { useState } from 'react';
import {Row, Col, Container} from 'react-bootstrap';

const Register = (props) => {

  const [userName, setName] = useState('');
  const [userPassword, setPassword] = useState('');
  const {onRouteChange} = props;

//getting username as input
  const onNameChange = (event) => {
    setName(event.target.value);
  }

//getting password as input
  const onPasswordChange = (event) => {
    setPassword(event.target.value);
  }


//user clicking on Regiter button- backend connection
  const onSubmitRegister = async() => {
    let userData = {
      userName: userName,
      userPassword: userPassword 
    }

    await fetch('http://localhost:8080/surabi/users/register', {
      method: 'POST',
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify(
        userData
      )
    })

    .then(
      response => response.json()
    )
    .then(
      data => { document.getElementById('registererr').style.display='none';
                console.log(data);
                onRouteChange('login');
              }
    )

    .catch(err => {
      document.getElementById('registererr').style.display='block';
    })

  }
  
  return (
      <Container >
      <Row className='justify-content-md-center'>
        <Col md={4} style={{marginTop:'50px', border: '3px solid darkgray', borderRadius:'10px', padding:'40px'}}>
            <legend style={{textAlign:'center', fontSize:'40px', padding:'3px', fontWeight:'bold'}}>REGISTER</legend>

            <Row className='justify-content-md-center' 
                 style={{padding:'5px', margin:'10px'}}>
              <label style={{fontSize:'20px'}}>Username</label>
              <input
                type="name"
                name="username"
                id="username"
                onChange={onNameChange}
                style={{fontSize:'15px'}}
              />
            </Row>
            <Row className='justify-content-md-center' 
                 style={{padding:'5px', margin:'10px'}}>
              <label style={{fontSize:'20px'}}>Password</label>
              <input
                type="password"
                name="password"
                id="password"
                onChange={onPasswordChange}
                style={{fontSize:'15px'}}
              />
            </Row>

            <Row className='justify-content-md-center'>
            <input
              onClick={onSubmitRegister}
              type="submit"
              value="Register"
              style={{color:'whitesmoke', fontSize:'20px', backgroundColor:'indigo', borderRadius:'5px', marginTop:'20px', height:'45px', width:'110px'}}
            />
            </Row>

{/*go to login page if existing user*/}
            <Row>
               <p onClick={() => onRouteChange('login')}  style={{textAlign:'center', color:'gray', fontSize:'15px', marginTop:'10px', cursor:'pointer'}}
                >Existing user? Login</p>
            </Row>
            
{/*display error for already registered user*/}
            <Row className='justify-content-md-center'>
                <p id='registererr' style={{display:'none', fontSize:'20px', marginTop:'10px', color:'red', textAlign:'center'}}>User already exists!</p>
            </Row>
    </Col>
    </Row>
    </Container>
    );
}
export default Register;
