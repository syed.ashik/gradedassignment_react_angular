import React, { useState } from 'react';
import Login from './components/Login';
import Register from './components/Register';
import Menu from './components/Menu';
import './App.css';


function App() {
 const [route, setRoute] = useState('login');
 const [userId, setUserId] = useState(0);

 const onRouteChange = (route) => {
    setRoute(route);    
  }

  const loadUser = (userId) => {
    setUserId(userId);
  }

  return (
    <div className="App">
      <section className='header'>
		      <p id='title'>Welcome  to  Surabi  Restaurant</p>
      </section>
      
      { (route === 'login') 
          ? <Login loadUser={loadUser} onRouteChange={onRouteChange}/>
          : (route === 'register')
              ? <Register onRouteChange={onRouteChange}/>
              : (route === 'menu')
                  ? <Menu userId={userId} onRouteChange={onRouteChange}/>
                  : <Login loadUser={loadUser} onRouteChange={onRouteChange}/>
      }

    </div>
  );
}

export default App;
