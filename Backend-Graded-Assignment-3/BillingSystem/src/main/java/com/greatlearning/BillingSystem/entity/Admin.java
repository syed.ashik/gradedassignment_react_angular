package com.greatlearning.BillingSystem.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "admin")
public class Admin {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "admin_id")
	private int adminId;
	
	@Column(name = "admin_name")
	private String adminName;
	
	@Column(name = "admin_password")
	private String adminPassword;
	
	@Column(name = "admin_login")
	private int adminLogIn;
	
	public Admin() {}
	

	public Admin(int adminId, String adminName, String adminPassword, int adminLogIn) {
		this.adminId = adminId;
		this.adminName = adminName;
		this.adminPassword = adminPassword;
		this.adminLogIn = adminLogIn;
	}


	public int getAdminId() {
		return adminId;
	}

	public void setAdminId(int adminId) {
		this.adminId = adminId;
	}

	public String getAdminName() {
		return adminName;
	}

	public void setAdminName(String adminName) {
		this.adminName = adminName;
	}

	public String getAdminPassword() {
		return adminPassword;
	}

	public void setAdminPassword(String adminPassword) {
		this.adminPassword = adminPassword;
	}
	
	public int getAdminLogIn() {
		return adminLogIn;
	}

	public void setAdminLogIn(int adminLogIn) {
		this.adminLogIn = adminLogIn;
	}


	@Override
	public String toString() {
		return "Admin [adminId = " + adminId + ", adminName = " + adminName + ", adminPassword = " + adminPassword
				+ ", adminLogIn = " + adminLogIn + "]";
	}
	
}
