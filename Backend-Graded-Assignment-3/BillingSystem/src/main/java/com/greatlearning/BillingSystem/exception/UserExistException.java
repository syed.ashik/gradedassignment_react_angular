package com.greatlearning.BillingSystem.exception;

@SuppressWarnings("serial")
public class UserExistException extends RuntimeException {
	
	UserExistException(){}

	public UserExistException(String str){
		super("*** "+str+" ***");
	}

}
