package com.greatlearning.BillingSystem.DAO;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

import org.hibernate.query.Query;
import javax.persistence.EntityManager;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.greatlearning.BillingSystem.entity.Menu;
import com.greatlearning.BillingSystem.entity.Order;
import com.greatlearning.BillingSystem.entity.User;

@Repository
public class UserDAOImpl implements UserDAO {
	
    private EntityManager entityManager;
	private float billAmount;
	
	
	@Autowired
	public UserDAOImpl(EntityManager entitymanager) {
		entityManager = entitymanager;
	}

	
//finding user by id	
	@Override
	public User findUser(int userId) {
		
		Session currentSession = entityManager.unwrap(Session.class);
		User user = currentSession.get(User.class,userId);
        return user;   
	}

	
	@Override
	public void save(User user) {
		
		Session currentSession = entityManager.unwrap(Session.class);
		currentSession.save(user);
	}
	

//checking user credentials 
	@Override
	public int authenticate(User user) {
		
		Session currentSession = entityManager.unwrap(Session.class);
		Query<User> query = currentSession.createQuery("from User where userName ='"+ user.getUserName() + "' and userPassword ='" + user.getUserPassword()+"'", User.class);
		List<User> users = query.getResultList();

		if(users.isEmpty())
		   return 0;
		else 
			return users.get(0).getUserId();
		
	}
	
	
	@Override
	public void setLogin(User user) {
		
		Session currentSession = entityManager.unwrap(Session.class);
		@SuppressWarnings("unchecked")
		Query<User> query = currentSession.createQuery("update User set userLogIn = 1 where userId = :id");
		query.setParameter("id", user.getUserId());
        query.executeUpdate();		
	}
	
		
	@Override
	public String setLogout(int userId) {
		
		Session currentSession = entityManager.unwrap(Session.class);	
		@SuppressWarnings("unchecked")
		Query<User> query = currentSession.createQuery("update User set userLogIn = 0 where userId = :id");
		query.setParameter("id", userId);
        query.executeUpdate();
        User user = currentSession.get(User.class,userId);
        return user.getUserName()+" Logged Out";
	}

	
	@Override
	public List<Menu> showMenu() {
		
		Session currentSession = entityManager.unwrap(Session.class);
		Query<Menu> query = currentSession.createQuery("from Menu", Menu.class);
		List<Menu> items = query.getResultList();
		return items;
	}
	
	
	@Override
	public String createOrder(int userId, Map<Integer,Integer> items) {
		
		Order order = new Order();
		Session currentSession = entityManager.unwrap(Session.class);		
		if(items.isEmpty()) {
			return "No item is selected..";
		}

		order.setUserId(userId);
		
        billAmount = 0;
        String bill = "SeqNumber---ItemName---Quantity---Price\n";
        
		for(Integer seqNumber : items.keySet()) {
			
			Query<Menu> query = currentSession.createQuery("from Menu where seqNumber ='"+ seqNumber + "'", Menu.class);
			List<Menu> item = query.getResultList();
			if(item.isEmpty()) {
				return "Order cannot be placed..Invalid item number is entered"; 
			}
			float price = item.get(0).getPrice();
			float t_price = (price * items.get(seqNumber));
		    bill += item.get(0).getSeqNumber()+"---"+item.get(0).getItemName()+"---"+items.get(seqNumber)+"---"+t_price+"\n";
			billAmount += t_price;
		}
	    
	    order.setBillAmount(billAmount);
			
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("uuuu/MM/dd");
		LocalDate localDate = LocalDate.now();
		order.setOrderDate(dtf.format(localDate));
	    
	    DateTimeFormatter dtf2 = DateTimeFormatter.ofPattern("HH:mm:ss");
		LocalTime localTime = LocalTime.now();
		order.setOrderTime(dtf2.format(localTime));
        
		currentSession.save(order);
		
		return "Order placed successfully\nYour Bill is here..\n\n" + bill + order;
	
	}
	
	
	@Override
	public void save(Order order) {
		
		Session currentSession = entityManager.unwrap(Session.class);
		currentSession.save(order);
	}

}
