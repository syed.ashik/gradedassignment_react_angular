package com.greatlearning.BillingSystem.AOP;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

@Aspect
@Component
public class LoggerAOP {

//Implementation of AROUND ADVICE AOP to log all features
    Logger log = LoggerFactory.getLogger(LoggerAOP.class);
	
	@Pointcut(value="execution(* com.greatlearning.BillingSystem.*.*.*(..) )")
	public void myPointcut() {
		
	}
	
	@Around("myPointcut()")
	public Object billingLogs(ProceedingJoinPoint pjp) throws Throwable {
		
		ObjectMapper mapper = new ObjectMapper();
		String methodName = pjp.getSignature().getName();
		String className = pjp.getTarget().getClass().toString();
		Object[] arguments = pjp.getArgs();
		
//Class names, methods called and their parameters are logged
		log.info(className + " : " + methodName + "()" + " Arguments : " + mapper.writeValueAsString(arguments));
		

		Object object = pjp.proceed();
		
//Class names, methods called and their parameters are logged
		log.info(className + " : " + methodName + "() " + " Response : " + mapper.writeValueAsString(object));
		
		return object;
	}
}
