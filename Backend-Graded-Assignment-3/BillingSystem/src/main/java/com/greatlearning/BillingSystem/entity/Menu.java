package com.greatlearning.BillingSystem.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "menu")
public class Menu {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "seq_number")
	private int seqNumber;
	
	@Column(name = "item_name")
	private String itemName;
	
	@Column(name = "category")
	private String category;
	
	@Column(name = "price")
	private float price;
	
	public Menu() {}

	public Menu(int seqNumber, String itemName, String category, float price) {
		this.seqNumber = seqNumber;
		this.itemName = itemName;
		this.category = category;
		this.price = price;
	}

	public int getSeqNumber() {
		return seqNumber;
	}

	public void setSeqNumber(int seqNumber) {
		this.seqNumber = seqNumber;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "Menu [seqNumber = " + seqNumber + ", itemName = " + itemName + ", category = " + category + ", price = " + price + "]";
	}

}
