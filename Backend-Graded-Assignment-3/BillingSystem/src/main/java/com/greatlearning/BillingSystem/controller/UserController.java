package com.greatlearning.BillingSystem.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.greatlearning.BillingSystem.entity.Menu;
import com.greatlearning.BillingSystem.entity.User;
import com.greatlearning.BillingSystem.exception.InvalidCredentialsException;
import com.greatlearning.BillingSystem.exception.NotFoundException;
import com.greatlearning.BillingSystem.exception.UserExistException;
import com.greatlearning.BillingSystem.service.UserService;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/surabi/users")
public class UserController {
	
		private UserService userService;
		
		@Autowired
		public UserController(UserService userservice) {
			userService = userservice;
		}

		
//user register - POST		
		@PostMapping("/register")
		public Object register(@RequestBody User user) {
	
			int userid = userService.authenticate(user);
			if(userid == 0) {
				userService.save(user);
				return user;
			}
			
			else{  //user defined exception if user is already registered
				try {
					throw new UserExistException("Already registered..Go to login");
				}
				catch(UserExistException ex) {
					return ex.getMessage();
				}
			}
		}


//user login - POST		
		@Transactional
		@PostMapping("/login")
		public Object login(@RequestBody User user) {
			
			int userid = userService.authenticate(user);
			if(userid != 0) {
				user.setUserId(userid);
				user.setUserLogIn(1);
				userService.setLogin(user);
				return user;
			}
			
			else {  //user defined exception for wrong username or password
				try {
					throw new InvalidCredentialsException("Wrong User Credentials");
				}
				catch(InvalidCredentialsException ex) {
					return ex.getMessage();
				}
			}
		}

		
//user logout - GET	
		@Transactional
		@GetMapping("/{userId}/logout")
		public Object logout(@PathVariable int userId) {
			return userService.setLogout(userId);
		}
		
//get menu - GET	
		@GetMapping("/menu")
		public List<Menu> showMenu(){
			return userService.showMenu();
		}
		
//place order and see bill - POST 	
		@PostMapping("/{userId}/order")
		public String selectItems( @PathVariable int userId, @RequestBody Map<Integer,Integer> items){
			
			User user = userService.findUser(userId);
			if(user != null)
			    return userService.createOrder(userId,items);	

			else {  //user defined exception
				try {
					throw new NotFoundException("User Not Found");
				}
				catch(NotFoundException ex) {
					return ex.getMessage();
				}
			}
		}

}
