package com.greatlearning.BillingSystem.exception;

@SuppressWarnings("serial")
public class InvalidCredentialsException extends RuntimeException {
	
	InvalidCredentialsException(){}

	public InvalidCredentialsException(String str){
		super("*** "+str+" ***");
	}
}
