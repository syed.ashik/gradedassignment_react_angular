package com.greatlearning.BillingSystem.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.greatlearning.BillingSystem.entity.Admin;
import com.greatlearning.BillingSystem.entity.Order;
import com.greatlearning.BillingSystem.entity.User;
import com.greatlearning.BillingSystem.exception.InvalidCredentialsException;
import com.greatlearning.BillingSystem.exception.NotFoundException;
import com.greatlearning.BillingSystem.exception.UserExistException;
import com.greatlearning.BillingSystem.service.AdminService;
import com.greatlearning.BillingSystem.service.UserService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/surabi/admin")
public class AdminController {
	
	private AdminService adminService;
	
	@Autowired
	public AdminController(AdminService adminservice, UserService userservice) {
		adminService = adminservice;
	}

//get all admins - GET
	@GetMapping("/")
	public List<Admin> getAllAdmins(){
		return adminService.findAllAdmins();
	}

	
//get admin by id - GET	
	@GetMapping("/{adminId}")
	public Object getAdminById(@PathVariable int adminId) {
		
		Admin admin = adminService.findAdmin(adminId);
		if(admin != null)
			return admin;
		
		else {  //user defined exception
			try {
				throw new NotFoundException("Admin Not Found");
			}
			catch(NotFoundException ex) {
				return ex.getMessage();
			}
		}
	}

	
//admin login - POST	
	@Transactional
	@PostMapping("/login")
	public Object login(@RequestBody Admin admin) {
		
		int adminid = adminService.authenticate(admin);
		if(adminid != 0) {
			admin.setAdminId(adminid);
			admin.setAdminLogIn(1);
			adminService.setLogin(admin);
			System.out.println(admin.getAdminName()+" Logged In");
			return admin;
		}				
		else {  //user defined exception
			try {
				throw new InvalidCredentialsException("Wrong Admin Credentials");
			}
			catch(InvalidCredentialsException ex) {
				return ex.getMessage();
			}
		}
	}
	

//admin logout - GET
	@Transactional
	@GetMapping("/{adminId}/logout")
	public String logout(@PathVariable int adminId) {
		return adminService.setLogout(adminId);
	}


//see today orders - GET	
	@GetMapping("/{adminId}/today-sales")
	public List<Order> showTodaySale(@PathVariable int adminId) {
		return adminService.todaySale();
	}

	
//see current month orders - GET
	@GetMapping("/{adminId}/month-sales")
	public List<Order> showMonthSale(@PathVariable int adminId) {
		return adminService.monthSale();
	}

	
//read all users - GET	
	@GetMapping("/users")
	public List<User> getAllUsers(){	
		return adminService.findAllUsers();
	}
	

//read user by id - GET
	@GetMapping("/users/{userId}")
	public Object getUserById(@PathVariable int userId) {
		
		User user = adminService.findUser(userId);
		if(user != null)
			return user;
		
		else {  //user defined exception
			try {
				throw new NotFoundException("User Not Found");
			}
			catch(NotFoundException ex) {
				return ex.getMessage();
			}
		}
	}


//create user - POST	
	@PostMapping("/add-user")
	public Object createUser(@RequestBody User user) {
		
		int userid = adminService.authenticate(user);
		if(userid == 0) {
			adminService.save(user);
			return user;
			//return "New user added successfully";
		}
		
		else{  //user defined exception
			try {
				throw new UserExistException("User already exists");
			}
			catch(UserExistException ex) {
				return ex.getMessage();
			}
		}
	
	}
	

//update existing user - PUT
	@Transactional
	@PutMapping("/update-user")
	public Object updateUser(@RequestBody User user) {
		
		User user1 = adminService.findUser(user.getUserId());
		if(user1 != null) {
			System.out.println(adminService.updateUser(user));
			return user;
		}
		
		else{  //user defined exception
			try {
				throw new NotFoundException("UserID not found");
			}
			catch(NotFoundException ex) {
				return ex.getMessage();
			}
		}
	
	}

	
//delete existing user - DELETE	
	@Transactional
	@DeleteMapping("/users/{userId}")
	public Object deleteUser(@PathVariable int userId) {
		
		User tempUser = adminService.findUser(userId);
		
		if(tempUser != null) {
			adminService.deleteUser(userId);
			return null;
			//return "Deleted User:\n" + tempUser;				
		}
		
		else {
			try {
				throw new NotFoundException("User Not Found");
			}
			catch(NotFoundException ex) {
				return ex.getMessage();
			}
		}
	}

}
