package com.greatlearning.BillingSystem.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "users")
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_id")
	private int userId;
	
	@Column(name = "user_name")
	private String userName;
	
	@Column(name = "user_password")
	private String userPassword;
	
	@Column(name = "user_login")
	private int userLogIn;
	

	public User() {}

	public User(int userId, String userName, String userPassword, int userLogIn) {
		this.userId = userId;
		this.userName = userName;
		this.userPassword = userPassword;
		this.userLogIn = userLogIn;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}
	
	public int getUserLogIn() {
		return userLogIn;
	}

	public void setUserLogIn(int userLogIn) {
		this.userLogIn = userLogIn;
	}

	@Override
	public String toString() {
		return "User [userId = " + userId + ", userName = " + userName + ", userPassword = " + userPassword + ", userLogIn = " + userLogIn + "]";
	}
	
}

