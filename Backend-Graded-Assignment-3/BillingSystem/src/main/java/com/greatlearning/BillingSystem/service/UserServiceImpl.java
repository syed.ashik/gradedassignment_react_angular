package com.greatlearning.BillingSystem.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatlearning.BillingSystem.DAO.UserDAO;
import com.greatlearning.BillingSystem.entity.Menu;
import com.greatlearning.BillingSystem.entity.Order;
import com.greatlearning.BillingSystem.entity.User;

@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserDAO userDAO;

	
	@Override
	public User findUser(int userId) {
		return userDAO.findUser(userId);
	}

	@Override
	public void save(User user) {
		userDAO.save(user);
	}

	@Override
	public int authenticate(User user) {		
		return userDAO.authenticate(user);
	}
	
	@Override
	public void setLogin(User user) {
		userDAO.setLogin(user);	
	}

	@Override
	public String setLogout(int userId) {
		return userDAO.setLogout(userId);
	}
	
	@Override
	public List<Menu> showMenu() {
		return userDAO.showMenu();
	}
	
	@Override
	public String createOrder(int userId, Map<Integer,Integer> items) {
		return userDAO.createOrder(userId,items);	
	}

	@Override
	public void save(Order order) {
		userDAO.save(order);
	}

}
