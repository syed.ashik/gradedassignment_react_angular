package com.greatlearning.BillingSystem.service;

import java.util.List;
import java.util.Map;

import com.greatlearning.BillingSystem.entity.Menu;
import com.greatlearning.BillingSystem.entity.Order;
import com.greatlearning.BillingSystem.entity.User;

public interface UserService {
	
	public User findUser(int userId);
	public void save(User user);
	
	public int authenticate(User user);
	public void setLogin(User user);
	public String setLogout(int userId);
	
	public void save(Order order);
	public List<Menu> showMenu();
	public String createOrder(int userId, Map<Integer,Integer> items);
}
