package com.greatlearning.BillingSystem.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatlearning.BillingSystem.DAO.AdminDAO;
import com.greatlearning.BillingSystem.entity.Admin;
import com.greatlearning.BillingSystem.entity.Order;
import com.greatlearning.BillingSystem.entity.User;


@Service
public class AdminServiceImpl implements AdminService{
	
	@Autowired
	private AdminDAO adminDAO;
	
	@Override
	public List<Admin> findAllAdmins() {
		return adminDAO.findAllAdmins();
	}
	
	@Override
	public Admin findAdmin(int adminId) {
		return adminDAO.findAdmin(adminId);
	}

	@Override
	public void save(Admin admin) {
		adminDAO.save(admin);
	}
	
	@Override
	public int authenticate(Admin admin) {
		return adminDAO.authenticate(admin);
	}
	
	@Override
	public void setLogin(Admin admin) {
		adminDAO.setLogin(admin);		
	}
	
	@Override
	public String setLogout(int adminId) {
		return adminDAO.setLogout(adminId);
	}

	@Override
	public List<Order> todaySale() {
		return adminDAO.todaySale();
	}

	@Override
	public List<Order> monthSale() {
		return adminDAO.monthSale();
	}
   
	@Override
	public int authenticate(User user) {
		return adminDAO.authenticate(user);
	}

	@Override
	public void save(User user) {
		adminDAO.save(user);	
	}

	@Override
	public List<User> findAllUsers() {
		return adminDAO.findAllUsers();
	}
	
	@Override
	public User findUser(int userId) {
		return adminDAO.findUser(userId);
	}
	

	@Override
	public String updateUser(User user) {
		return adminDAO.updateUser(user);
	}

	@Override
	public void deleteUser(int userId) {
		adminDAO.deleteUser(userId);		
	}

}
