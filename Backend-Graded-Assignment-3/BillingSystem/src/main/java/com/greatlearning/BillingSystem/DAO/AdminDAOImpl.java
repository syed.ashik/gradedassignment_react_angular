package com.greatlearning.BillingSystem.DAO;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.greatlearning.BillingSystem.entity.Admin;
import com.greatlearning.BillingSystem.entity.Order;
import com.greatlearning.BillingSystem.entity.User;

@Repository
public class AdminDAOImpl implements AdminDAO {
	
	private EntityManager entityManager;	
	
	@Autowired
	public AdminDAOImpl(EntityManager entitymanager) {
		entityManager = entitymanager;
	}

//get all admins   
	@Override
	public List<Admin> findAllAdmins() {

		Session currentSession = entityManager.unwrap(Session.class);
		Query<Admin> query = currentSession.createQuery("from Admin", Admin.class);
		List<Admin> admins = query.getResultList();
		return admins;
	}


//get one admin by adminId
	@Override
	public Admin findAdmin(int adminId) {

		Session currentSession = entityManager.unwrap(Session.class);
		Admin admin = currentSession.get(Admin.class,adminId);
        return admin; 
	}


	@Override
	public void save(Admin admin) {
		Session currentSession = entityManager.unwrap(Session.class);
		currentSession.save(admin);
		
	}

	
//checking admin login credentials
	@Override
	public int authenticate(Admin admin) {
		
		Session currentSession = entityManager.unwrap(Session.class);
		Query<Admin> query = currentSession.createQuery("from Admin where adminName ='"+ admin.getAdminName() + "' and adminPassword ='" + admin.getAdminPassword()+"'", Admin.class);
		List<Admin> admins = query.getResultList();

		if(admins.isEmpty())
		   return 0;
		else 
			return admins.get(0).getAdminId();
		
	}
	
	
	@Transactional
	@Override
	public void setLogin(Admin admin) {
		
		Session currentSession = entityManager.unwrap(Session.class);
		@SuppressWarnings("unchecked")
		Query<Admin> query = currentSession.createQuery("update Admin set adminLogIn = 1 where adminId = :id");
		query.setParameter("id", admin.getAdminId());
        query.executeUpdate();	
		
	}

	@Transactional
	@Override
	public String setLogout(int adminId) {
		
		Session currentSession = entityManager.unwrap(Session.class);	
		@SuppressWarnings("unchecked")
		Query<Admin> query = currentSession.createQuery("update Admin set adminLogIn = 0 where adminId = :id");
		query.setParameter("id", adminId);
        query.executeUpdate();
        Admin admin = currentSession.get(Admin.class,adminId);
        return admin.getAdminName()+" Logged Out";
	}


//get today sales
	@Override
	public List<Order> todaySale() {
		
		Session currentSession = entityManager.unwrap(Session.class);
		String today_orders = "TODAY SALES..\n";
		float today_income = 0;
		
		Query<Order> query = currentSession.createQuery("from Order where orderDate = '" + LocalDate.now() + "'", Order.class);
		List<Order> orders = query.getResultList();
		
		if(orders.isEmpty()) {
			System.out.println("No sales yet!");
			return null;
		}
		
		for(Order order: orders) {
			today_orders += order;
			today_income += order.getBillAmount();		
		}
		
		System.out.println(today_orders + "\nTODAY'S INCOME: Rs." + today_income);
		return orders;
	}


//get this month sales
	@Override
	public List<Order> monthSale() {
		
		Session currentSession = entityManager.unwrap(Session.class);
		String month_orders = "THIS MONTH'S SALE..\n";
		float month_income = 0;
		
		Query<Order> query = currentSession.createQuery("from Order where monthname(orderDate) = monthname(curdate())", Order.class);
		List<Order> orders = query.getResultList();
		
		if(orders.isEmpty()) {
			System.out.println("No sales yet!");
			return null; 
		}
		
		for(Order order: orders) {
			month_orders += order;
			month_income += order.getBillAmount();		
		}

		System.out.println(month_orders + "\nTHIS MONTH'S INCOME: Rs." + month_income);
		return orders;
	}


	@Override
	public int authenticate(User user) {
		
		Session currentSession = entityManager.unwrap(Session.class);
		Query<User> query = currentSession.createQuery("from User where userName ='"+ user.getUserName() + "' and userPassword ='" + user.getUserPassword()+"'", User.class);
		List<User> users = query.getResultList();

		if(users.isEmpty())
		   return 0;
		else 
			return users.get(0).getUserId();
	}


	@Override
	public void save(User user) {
		Session currentSession = entityManager.unwrap(Session.class);
		currentSession.save(user);
		
	}

	
//get all users	
	@Override
	public List<User> findAllUsers() {
		
		Session currentSession = entityManager.unwrap(Session.class);
		Query<User> query = currentSession.createQuery("from User",User.class);
		List<User> users = query.getResultList();
		return users;
	}


//get one user by userId
	@Override
	public User findUser(int userId) {
		
		Session currentSession = entityManager.unwrap(Session.class);
		User user = currentSession.get(User.class,userId);
        return user;
	}
	

//update existing user 
	@Override
	public String updateUser(User user) {
		
		Session currentSession = entityManager.unwrap(Session.class);
	
		@SuppressWarnings("unchecked")
		Query<User> query1 = currentSession.createQuery("update User set userName = :name where userId = :id");
		query1.setParameter("name", user.getUserName());
		query1.setParameter("id", user.getUserId());
        query1.executeUpdate();
        
        @SuppressWarnings("unchecked")
		Query<User> query2 = currentSession.createQuery("update User set userPassword = :password where userId = :id");
		query2.setParameter("password", user.getUserPassword());
		query2.setParameter("id", user.getUserId());
        query2.executeUpdate();
        
        return "User with ID:" + user.getUserId() + " updated successfully";
	}

	
//delete existing user
	@Override
	public void deleteUser(int userId) {
		
		Session currentSession = entityManager.unwrap(Session.class);
		@SuppressWarnings("unchecked")
		Query<User> query = currentSession.createQuery("delete User where userId = :id");
		query.setParameter("id", userId);
		query.executeUpdate();
		
	}

}
