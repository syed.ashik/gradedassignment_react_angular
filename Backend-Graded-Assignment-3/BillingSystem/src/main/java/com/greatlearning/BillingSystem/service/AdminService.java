package com.greatlearning.BillingSystem.service;

import java.util.List;

import com.greatlearning.BillingSystem.entity.Admin;
import com.greatlearning.BillingSystem.entity.Order;
import com.greatlearning.BillingSystem.entity.User;

public interface AdminService {
	
	public List<Admin> findAllAdmins();
	public Admin findAdmin(int adminId);
	public void save(Admin admin);
	
	public int authenticate(Admin admin);
	public void setLogin(Admin admin);
	public String setLogout(int adminId);
	
	public List<Order> todaySale();
	public List<Order> monthSale();
	
	public int authenticate(User user);
	public List<User> findAllUsers();
	public void save(User user);
	public User findUser(int userId);
	public String updateUser(User user);
	public void deleteUser(int userId);
	
}
