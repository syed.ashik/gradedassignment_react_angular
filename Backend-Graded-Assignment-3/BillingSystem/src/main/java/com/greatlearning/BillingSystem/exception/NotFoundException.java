package com.greatlearning.BillingSystem.exception;

@SuppressWarnings("serial")
public class NotFoundException extends RuntimeException {
	
	NotFoundException(){}

	public NotFoundException(String str){
		super("*** "+str+" ***");
	}
}