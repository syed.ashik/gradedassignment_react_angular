package com.greatlearning.BillingSystem.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "orders")
public class Order {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "order_id")
	private int orderId;
	
	@Column(name = "user_id")
	private int userId;
	
	@Column(name = "bill_amount")
	private float billAmount;
	
	@Column(name = "order_date")
	private String orderDate;
	
	@Column(name = "order_time")
	private String orderTime;
	
	
	public Order() {}

	public Order(int orderId, int userId, float billAmount, String orderDate, String orderTime) {
		this.orderId = orderId;
		this.userId = userId;
		this.billAmount = billAmount;
		this.orderDate = orderDate;
		this.orderTime = orderTime;
	}

	public int getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public float getBillAmount() {
		return billAmount;
	}

	public void setBillAmount(float billAmount) {
		this.billAmount = billAmount;
	}

	public String getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}

	public String getOrderTime() {
		return orderTime;
	}

	public void setOrderTime(String orderTime) {
		this.orderTime = orderTime;
	}
	
	@Override
	public String toString() {
		return "\n[ orderId = " + orderId + ", userId = " + userId + ", orderDate = "
				+ orderDate + ", orderTime = " + orderTime + ", \nTotal Amount = Rs." + billAmount + " ]\n";
	}
	
}
