OPEN README.md in WEB IDE for proper alignment.
The following are comments on this assignment.

**EXECUTION**
1. Run Share.sql file using source command
2. To run code, BillingSystem\src\main\java\com\greatlearning\BillingSystem\BillingSystemApplication.java


**DATABASE**
1. DDL and DML queries are in Share.sql file
2. Database name - surabi
3. Four tables created - admin, users, menu, orders
4. There are two admins and no new admin can be added.


**CODE IMPLEMENTATION**
1. Admin and User(customer) are the two controllers.




**ADMIN:** 
/surabi/admin/            	          - Get all admins
/surabi/admin/{adminId}  	          - Get one admin
/surabi/admin/login        	          - Post Admin login
/surabi/admin/{adminId}/logout        - Get Admin logout
/surabi/admin/{adminId}/today-sales   - Get today's sale
/surabi/admin/{adminId}/month-sales   - Get current month's sale

**CRUD operations:**
/surabi/admin/users		        - Get all users 
/surabi/admin/users/{userId}	- Get one user
/surabi/admin/add-user		    - Post Create new user
/surabi/admin/update-user   	- Put Update existing user (userId, userName, userPassword must be given in body)
/surabi/admin/users/{userId}	- Delete existing user


**USER:**
/surabi/users/register      	- Post register user
/surabi/users/login         	- Post user login
/surabi/users/{userId}/logout   - Get user logout
/surabi/users/menu 		        - Get menu
/surabi/users/{userId}/order	- Post place order and view bill 
                                  JSON body example to place order: {"2":"2", "6":"3", "8":"1"}
                                  Key is item id and Value is number of items needed.
