CREATE database surabi;

USE surabi;

CREATE table admin(
admin_id int primary key,
admin_name varchar(20),
admin_password varchar(20),
admin_login int default 0);


CREATE table users(
user_id int primary key auto_increment,
user_name varchar(20),
user_password varchar(20),
user_login int default 0);


CREATE table menu(
seq_number int primary key auto_increment,
item_name varchar(25),
category varchar(20),
price float);


CREATE table orders(
order_id int primary key auto_increment,
user_id int,
bill_amount float,
order_date date,
order_time time,
foreign key(user_id) references users(user_id));


ALTER table users auto_increment=0;
ALTER table menu auto_increment=0;
ALTER table orders auto_increment=0;


INSERT into admin values(101,'AdminOne','Admin@123');
INSERT into admin values(102,'AdminTwo','Admin@456');

INSERT into users values(1,'Ashik','Ashik123');
INSERT into users values(2,'Babu','Babu456');
INSERT into users values(3,'Cibi','Cibi789');
INSERT into users values(4,'Dany','Dany444');
INSERT into users values(5,'Elan','Elan555');

INSERT into menu values(1,'French Fries','veg',49.90);
INSERT into menu values(2,'Pav Bhajji','veg',30.50);
INSERT into menu values(3,'Chicken Nuggets','nonveg',65.50);
INSERT into menu values(4,'Mutton Briyani','nonveg',170.00);
INSERT into menu values(5,'Cheese Pizza','veg',190.00);
INSERT into menu values(6,'Barbeque Pizza','nonveg',240.00);
INSERT into menu values(7,'Redsauce Pasta','veg',169.50);
INSERT into menu values(8,'Shezwan Noodles','nonveg',115.00);
INSERT into menu values(9,'Chicken Burger','nonveg',100.00);
INSERT into menu values(10,'Garlic Breads','veg',75.50);

INSERT into orders values(1,2,321,'2021-07-16','10:21:43');
INSERT into orders values(2,1,211.5,'2021-08-21','13:40:11');
INSERT into orders values(3,4,600.5,'2021-08-29','12:21:31');
INSERT into orders values(4,3,701.49,'2021-08-09','19:14:21');
INSERT into orders values(5,5,285,'2021-09-01','09:31:33');
INSERT into orders values(6,3,190,'2021-09-02','15:00:16');
INSERT into orders values(7,5,330.9,'2021-09-05','10:55:56');
INSERT into orders values(8,1,222,'2021-09-08','07:00:50');
INSERT into orders values(9,4,560.5,'2021-09-08','10:40:39');
INSERT into orders values(10,2,106,'2021-09-08','12:09:56');




